package com.babestudios.dozetester;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

import com.sasnee.scribo.DebugHelper;

import static android.content.Context.POWER_SERVICE;

public class DozeReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		String action = null;
		try {
			action = intent.getAction();
		} catch (Exception e) {
			e.printStackTrace();
		}
		DebugHelper.init(context, "CapturedLogs.txt", false);
		if(action != null && action.equals(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED)){
			PowerManager manager = (PowerManager) context.getSystemService(POWER_SERVICE);
			if(manager.isIgnoringBatteryOptimizations("com.babestudios.dozetester")) {
				DebugHelper.logRequest("test", "Ignoring battery optimizations.");
			} else {
				DebugHelper.logRequest("test", "Not ignoring battery optimizations.");
			}
			if(manager.isDeviceIdleMode()) {
				DebugHelper.logRequest("test", "Doze mode on.");
			} else {
				DebugHelper.logRequest("test", "Doze mode off.");
			}
		} else {
			DebugHelper.logRequest("test", "DozeReceiver received");
			context.startService(new Intent(context, DozeService.class));
		}
	}
}

