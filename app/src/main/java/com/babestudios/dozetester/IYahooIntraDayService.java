package com.babestudios.dozetester;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface IYahooIntraDayService {
	@GET(BuildConfig.YAHOO_INTRA_DAY_ENDPOINT)
	Observable<YahooIntraDayStockResponse> getIntraDayStockPrices(@Path("ticker") String ticker,
																  @Path("days") String days);
}
