package com.babestudios.dozetester;

import com.google.gson.annotations.SerializedName;

public class YahooIntraDayStockResponse {
		public Meta meta;
		@SerializedName("TimeStamp-Ranges")
		public TimeStampRange[] timeStampRanges;
		public TimeRange Timestamp;
		public long[] labels;
		public PriceRanges ranges;
		public StockPriceSeriesItem[] series;
}
