package com.babestudios.dozetester;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.sasnee.scribo.DebugHelper;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class DozeService extends Service implements Observer<YahooIntraDayStockResponse> {

	private Handler handler = new Handler();

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	private Retrofit retroFit;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		retroFit = createRetrofit();
		DebugHelper.init(this, "CapturedLogs.txt", false);
		DebugHelper.logRequest("test", "DozeService started");
		doPeriodicWork();
		return super.onStartCommand(intent, flags, startId);

	}

	private void doPeriodicWork() {
		IYahooIntraDayService service = retroFit.create(IYahooIntraDayService.class);
		service.getIntraDayStockPrices("icp.l", "1").subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(this);
	}

	private Retrofit createRetrofit() {
		return new Retrofit.Builder()//
				.baseUrl(BuildConfig.YAHOO_INTRA_DAY_BASE_URL)//
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();
	}

	@Override
	public void onCompleted() {
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				doPeriodicWork();
			}
		}, 60 * 1000);
	}

	@Override
	public void onError(Throwable e) {
		DebugHelper.logRequest("test", "Error in network call: " + e.getLocalizedMessage());
	}

	@Override
	public void onNext(YahooIntraDayStockResponse yahooIntraDayStockResponse) {
		double close = yahooIntraDayStockResponse.series[yahooIntraDayStockResponse.series.length - 1].close;
		DebugHelper.logRequest("test", "Latest price is: " + close);
	}
}
