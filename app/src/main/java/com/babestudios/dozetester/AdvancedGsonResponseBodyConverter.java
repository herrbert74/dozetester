package com.babestudios.dozetester;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;


final class AdvancedGsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {
	private final Gson gson;
	private final TypeAdapter<T> adapter;

	AdvancedGsonResponseBodyConverter(Gson gson, TypeAdapter<T> adapter) {
		this.gson = gson;
		this.adapter = adapter;
	}

	@Override
	public T convert(ResponseBody body) throws IOException {
		String dirty = body.string();
		String clean = dirty.replaceAll("finance_charts_json_callback\\(", "");
		clean = clean.replaceAll("\\)$", "");
		try {
			return adapter.fromJson(clean);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			body.close();
		}
	}
}
