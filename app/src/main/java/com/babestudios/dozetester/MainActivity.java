package com.babestudios.dozetester;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.sasnee.scribo.DebugHelper;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {

	private static final int REQUEST_WRITE_STORAGE = 0;

	DozeReceiver receiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setBroadcastReceiver();
		Button button = (Button) findViewById(R.id.button);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (ContextCompat.checkSelfPermission(MainActivity.this, WRITE_EXTERNAL_STORAGE)
						== PackageManager.PERMISSION_GRANTED) {
					doStuff();
				} else {
					ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
				}
			}
		});

		Intent intent = new Intent();
		String packageName = getPackageName();
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		if (!pm.isIgnoringBatteryOptimizations(packageName)) {
			intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
			intent.setData(Uri.parse("package:" + packageName));
			startActivity(intent);
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	private void setBroadcastReceiver() {
		receiver = new DozeReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED);
		registerReceiver(receiver, filter);
	}

	private void doStuff() {
		File file = new File(Environment.getExternalStorageDirectory() + File.separator + "CapturedLogs.txt");
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		DebugHelper.init(getApplicationContext(), "CapturedLogs.txt", false);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis() + 60 * 1000);
		AlarmManager alarmManager = (AlarmManager) MainActivity.this.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(MainActivity.this, DozeReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 100001, intent, 0);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			DebugHelper.logRequest("test", "Scheduling start alarm to (allow idle): " + DateUtil.formatLongDateFromTimeStampMillis(calendar.getTimeInMillis()));
			alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
		} else {
			DebugHelper.logRequest("test", "Scheduling start alarm to (only set): " + DateUtil.formatLongDateFromTimeStampMillis(calendar.getTimeInMillis()));
			alarmManager.setExact(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
		}
	}


	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		//Checking the request code of our request
		if (requestCode == REQUEST_WRITE_STORAGE) {

			//If permission is granted
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				doStuff();
				//Displaying a toast
				Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();

			} else {
				//Displaying another toast if permission is not granted
				Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	protected void onDestroy() {
		unregisterReceiver(receiver);
		super.onDestroy();
	}
}
