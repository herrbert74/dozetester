package com.babestudios.dozetester;

import com.google.gson.annotations.SerializedName;

public class Meta {
	public String uri;
	public String ticker;
	@SerializedName("Company-Name")
	public String companyName;
	@SerializedName("Exchange-name")
	public String exchangeName;
	public String unit;
	public String timezone;
	public String currency;
	public int gmtoffset;
	public double previous_close;
}
