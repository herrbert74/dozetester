# What is this repository for? #

This repository was set up to test Doze mode and particularly how alarms, services and background networking work while in Doze mode introduced in Marshmallow.

## Quick summary ##

There is only one button on the User Interface, which will start an alarm in a minute. The broadcast receiver will start a background service which will periodically download data from the internet. Every step and the network result will be printed on logcat and the same time saved on the SD card. The time stamps help to determine if the app works as expected.
The outcome can be changed by changing the setExactAndAllowWhileIdle call to setExact and by adding the app to the battery whitelist in the device settings.

## Useful links ##

[Testing the app with Doze](https://developer.android.com/training/monitoring-device-state/doze-standby.html#testing_doze)

[Whitelisting the app for Doze mode](https://developer.android.com/training/monitoring-device-state/doze-standby.html#support_for_other_use_cases)

## Conclusion ##

#### Without whitelisting ####
1. Once the service started it will always run, even in Doze mode.
2. The alarm will be deferred until we come out of Doze mode.
3. Networking doesn't work.

#### With whitelisting ####
1. The alarm will be triggered at the expected time even in Doze mode.
2. Networking works, but there might be large delays.

It seems that requesting the permission for ignoring the optimisations will whitelist the app, but the Settings app is not updated (our app is not on th ewhitelist) until the user stops and restarts it.
